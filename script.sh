#!/bin/bash
# Auto generated code

if [[ -z "$SCRIPT" ]]; then
    echo "entrypoint for the main.yml is not defined. Please define the entrypoint variable"
    STATE=1
else
    bash -c "$SCRIPT"

    STATE=$?

    if [ $STATE -eq 0 ] ; then
      echo 'SUCCEEDED'
    else
      echo 'FAILED'
    fi
fi

exit $STATE
