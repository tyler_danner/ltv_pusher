#!/usr/bin/env python
# coding: utf-8

# In[1]:


from lib_aunsight import context, lib
from dslib.ioutils import aunsight_connector
from lib_aunsight.context import AunsightContext
import boto3, botocore
from datetime import timedelta, date
import sys

#Environment variables
S3_ACCESS_sGUID = 'ee8ec909-e318-46d8-bddc-a6fb4b0fa8af'
S3_SECRET_sGUID = '32fee668-7a83-4b1d-97a9-9b8ff38bf904'
S3_BUCKET_sGUID = 'b6b8de65-f013-4d3d-bdc4-b3b4b8459ad2'
S3_USER_sGUID = 'b6062fb8-a43d-4c18-97b6-37e8b82f802f'

TOKEN = '653138ca-7b81-4790-bffe-a8fb3c99638c'
HOST = 'https://api.aunsight.com'


S3_GENERIC_WORKFLOW_GUID = '88f71f7c-2ca5-4753-a8ec-22c4851b690c'
MEMENTO = '9c5360ab-3175-4801-8c7c-ba5e909b6865'
ERROR = '1edf6e43-639e-44f8-89e9-ffd82c1b0b32'

#AU2 context
c = context.AunsightContext(token=TOKEN, api_rest_host=HOST)

guid_dict = {
    'wc_service_model_master': '569c0336-b43d-4bde-afd2-dc1b97d4c501',
    'wc_household_d': '569c0336-b43d-4bde-afd2-dc1b97d4c501',
    'wc_asset_d': '7ebe169b-ab99-4afa-87b0-0d56e90af638',
    'wc_model_master': '9c4c2e72-e3ec-4c12-83a5-f42b64507064',
    'wc_asset_f2': '1f5d82c3-b899-4fae-b8e5-33d822bb94e1',
    'wc_person_d': '9ca4b48a-0078-452e-93fb-ccd728c434fc',
    'buss_f_name': '614a39ff-fc64-4826-91a0-6e0ed022f55c',
    'buss_l_name': '1c1ac213-be92-4452-a9d4-a6e41d9adf08'
}

today = date.today()

PUBLIC_KEY=str(lib.models.Secret(c, S3_ACCESS_sGUID).download().text)
SECRET_KEY=str(lib.models.Secret(c,S3_SECRET_sGUID).download().text)
S3_BUCKET_NAME = str(lib.models.Secret(c,S3_BUCKET_sGUID).download().text).strip()
sys.stdout.write(str(S3_BUCKET_NAME))

def s3_check(path):
    session = boto3.Session(
    aws_access_key_id=PUBLIC_KEY,
    aws_secret_access_key=SECRET_KEY
    )

    b = session.resource('s3').Bucket(S3_BUCKET_NAME)
    
    sys.stdout.write('checking for' + str(path['FILEPATH']))
    
    if (bool(list(b.objects.filter(Prefix=path['FILEPATH']).limit(1)))):
        for i in b.objects.filter(Prefix=path['FILEPATH']).limit(1):
            ret_bytes = i.size
        return True, ret_bytes
    else:
        return False, 0

def add_to_mementos(path, size, bucket, date):
    memento = c.memento()
    memento.set('series', MEMENTO)
    memento.set('value', '\t'.join(['path:'+str(path), 'size:'+str(size), 'bucket:'+str(bucket),'date_added'+str(date)]))
    memento.create()


# In[2]:


error = c.atlas_record(ERROR)
s3_generic_workflow = c.workflow(S3_GENERIC_WORKFLOW_GUID)
    
for table,guid in guid_dict.items():
    temp = c.atlas_record(guid)
    
    try:
        parameters = {'ATLAS_RECORD':temp.id,
            'S3_BUCKET_SECRET_ID':S3_BUCKET_sGUID,
            'S3_SECRET_SECRET_ID':S3_SECRET_sGUID,
            'S3_ACCESS_SECRET_ID':S3_ACCESS_sGUID,
            'S3_USERID_SECRET_ID':S3_USER_sGUID,
            'FILEPATH':'ltv_aunsight_'+table,
            'PROCESS_RESOURCE': 'PROCESS_RANCHER_SHARED',
            'USE_HEADERS': 'True'
        }
        
        today = str(date.today())
        workflow_job = s3_generic_workflow.submit(parameters=parameters)
        workflow_job.wait()
        state = workflow_job.get('state')
        sys.stdout.write(str(state))
        if (state == 'SUCCEEDED'):
            present, num_bytes = s3_check(parameters)
            if (present and num_bytes>0):
                add_to_mementos(parameters['FILEPATH'], num_bytes, S3_BUCKET_NAME, today)
                with open('wc_buss_pusher_progress.txt', 'a+') as progress:
                    progress.write('Success on'+parameters['FILEPATH'])
            else:
                error.upload('Not added on ltv_aunsight_'+table+'date: '+today+'\n', write_mode='append')
                sys.stdout.write('Not added' + str(present), str(num_bytes))
                with open('wc_buss_pusher_progress.txt', 'a+') as progress:
                    progress.write('Failure on '+parameters['FILEPATH']+ str(present)+ str(num_bytes))
            
        else:
            error.upload('status non-success on ltv_aunsight_'+table+'date: '+today+'\n', write_mode='append')
            sys.stdout.write('status non-Success')
            with open('wc_buss_pusher_progress.txt', 'a+') as progress:
                    progress.write('Failure on '+str(parameters['FILEPATH'])+ ' Could not find file in S3')

    except Exception as e:
        error.upload('Error on ltv_aunsight_'+table+str(e)+'date: '+today+'\n', write_mode='append')
        with open('wc_buss_pusher_progress.txt', 'a+') as progress:
            progress.write('Error on ltv_aunsight_'+table+str(e))
            sys.stdout.write('Error in writing'+str(e))
